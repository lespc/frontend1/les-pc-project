import './App.scss';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Quizz from './pages/Quizz'
import AboutUs from './pages/AboutUs'
import {AnimatePresence} from "framer-motion"
import Footer from './components/Footer/Footer'

function App() {
  return (
    <>
      <Router>
        <Switch>
        <AnimatePresence exitBeforeEnter>
          <Route exact component={Home} path="/" />
          <Route exact component={AboutUs} path="/about-us" />
          <Route exact component={Quizz} path="/quizz" />

        </AnimatePresence>
        </Switch>
      </Router>
    </>
  );
}

export default App;
