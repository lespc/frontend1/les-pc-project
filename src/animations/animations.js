export const animationPlay = {
    in:{
        opacity:0.5,

    },
    out:{
        opacity:1,
        y:0,
        transition:{ delay: 0.5, duration: 1 },
        
    },
    end:{
        y:0,
        opacity:1,
    },
}

export const svgVariants = {
    visible: {
      rotate: 0,
      transition: { duration: 1 },
    },
  };
  
  export const pathVariants = {
    hidden: {
      opacity: 1,
      pathLength: 0,
    },
    visible: {
      opacity: 1,
      pathLength: 1,
      transition: {
        duration: 2,
        ease: "easeInOut",
      },
    },
  };
  