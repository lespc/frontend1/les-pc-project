import React from "react";
import "./Dragzone.scss";
import { motion } from "framer-motion";
import {render} from "@testing-library/react"
import Toaster from "../Toaster/Toaster"; 


const DragZone = ({ item, categorie, name, toastMsg, setPourcent, pourcent, pTop, pBottom, pRight, pLeft }) => {

  if(categorie === "plastique"){
    setPourcent(pourcent + 5)
  }
  return (
    <>
        <motion.div
          drag
          className="déchet"
          onTap={() => render(<Toaster info toastMsg={toastMsg} />)}
        >
          <h4>{name}</h4>
          <img src={item} alt="IMG" />
        </motion.div>
    </>
  );
};

export default DragZone;
