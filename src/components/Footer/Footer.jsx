import React from 'react'
import "./Footer.scss"

const Footer = (props) => {
  return (
    <>
      <div className="footer-ecologiax" style={{backgroundColor: props.backgroundColor}}>
        <strong>© 2021 Ecologiax. Tous droits résérvés</strong>
      </div>
    </>
  )
}

export default Footer
