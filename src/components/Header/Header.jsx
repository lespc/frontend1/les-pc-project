import React from 'react'
import "./Header.scss"

const Header = () => {
    return (
        <>
            <div className="header-img">
                {/* image */}
            </div>
            <div className="header-text">
                Proteger la planete c'est proteger les generations futures
            </div>
        </>
    )
}

export default Header
