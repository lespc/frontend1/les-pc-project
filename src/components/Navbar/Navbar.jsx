import React, { useState } from 'react'
import "./Navbar.scss"
import { NavLink, useHistory } from 'react-router-dom'
import LogoSVG from '../SVG/LogoSVG'
import Sidebar from '../Sidebar/Sidebar'
import { FcMenu } from 'react-icons/fc';

const Navbar = (props) => {
  const history = useHistory()
  const [showSidebar, setShowSidebar] = useState(false)

  return (
    <>
      <div className="navbar-ecologiax" style={{backgroundColor: props.backgroundColor}}>
        <div className="logo-wrapper">
          <LogoSVG onClick={() => history.push("/")} />
        </div>

        <div className="navlinks">
          <NavLink to="/" className="links">Home</NavLink>
          <NavLink to="/quizz" className="links">Quizz</NavLink>
          <NavLink to="/about-us" className="links">A propos</NavLink>
        </div>
        <FcMenu className="btn-show-sidebar" onClick={() => setShowSidebar(true)} />
      </div>
      <Sidebar
        isShow={showSidebar}
        onClose={() => setShowSidebar(false)} />
    </>
  )
}

export default Navbar
