import React from 'react'
import "./Poubelle.scss"

const Poubelle = ({trash, categorie}) => {
    return (
        <>
            <img className={`${categorie} poubelle`} src={trash} alt="poubelle" />
        </>
    )
}

export default Poubelle
