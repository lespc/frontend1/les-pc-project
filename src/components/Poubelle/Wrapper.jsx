import React from 'react'
import "./Wrapper.scss"
import Poubelle from './Poubelle'
import Poubelle1 from '../../images/p-metal.PNG'
import Poubelle2 from '../../images/p-papier.PNG'
import Poubelle3 from '../../images/p-plastique.PNG'
import Poubelle4 from '../../images/p-verre.PNG'
import Poubelle5 from '../../images/p-black.jfif'

const Wrapper = () => {
    return (
        <>
            <div className="wrapper-poubelles">
                <Poubelle categorie="metal" trash={Poubelle1} />
                <Poubelle categorie="papier" trash={Poubelle2} />
                <Poubelle categorie="plastique" trash={Poubelle3} />
                <Poubelle categorie="verre" trash={Poubelle4} />
                <Poubelle categorie="organique" trash={Poubelle5} />
            </div>
        </>
    )
}

export default Wrapper
