import React from 'react'
import { ProgressBar } from 'react-bootstrap';
import './EarthProgressBar.scss'

const EarthProgressBar = (props) => {
  let variant = 'success';
  if (props.progress < 40) {
    variant = 'danger'
  } else if (props.progress < 70) {
    variant = 'warning'
  }

  return (
    <div className="earth-progress-bar">
      <div className="progress-number">{props.progress} %</div>
      <ProgressBar variant={variant} now={props.progress} />
    </div>
  )
}

export default EarthProgressBar
