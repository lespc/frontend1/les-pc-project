import React from 'react'
import "./Sidebar.scss"
import { NavLink } from 'react-router-dom'
import { IoClose } from 'react-icons/io5';

const Sidebar = (props) => {
  return (
    <div className={props.isShow ? 'sidebar' : 'sidebar sidebar-hide'}>
      <h1>Ecologiax</h1>
      <p><NavLink to="/" className="links">Accueil</NavLink></p>
      <p><NavLink to="/quizz" className="links">Quizz</NavLink></p>
      <p><NavLink to="/about-us" className="links">A propos</NavLink></p>
      <div className="close-sidebar" onClick={props.onClose}>
        <IoClose />
        <span>Close</span>
      </div>
    </div>
  )
}

export default Sidebar