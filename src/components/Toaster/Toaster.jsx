import React, { useEffect } from "react";
import "./Toaster.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { cleanup } from "@testing-library/react";

const Toaster = (props) => {
  const notifyErr = () => toast.error(props.toastMsg, {toastId: props.id, className: props.classNameToast, autoClose: 6000});
  const notifyInfo = () => toast.info(props.toastMsg, {toastId: props.id, className: props.classNameToast, autoClose: 6000});
  const notifyWarn = () => toast.warn(props.toastMsg, {toastId: props.id, className: props.classNameToast, autoClose: 6000});
  const notifySuccess = () => toast.success(props.toastMsg, {toastId: props.id, className: props.classNameToast, autoClose: 6000});
  const notifyDark = () => toast.dark(props.toastMsg, {toastId: props.id, className: props.classNameToast, autoClose: 6000});

  useEffect(() => {
      if(props.error) notifyErr()
      if(props.info) notifyInfo()
      if(props.warn) notifyWarn()
      if(props.success) notifySuccess()
      if(props.dark) notifyDark()

      const timer = setTimeout(() => {
        cleanup()
      }, 6500);
      return () => clearTimeout(timer);
  }, [])
  return (
    <>
      <div>
        <ToastContainer limit={1}  />
      </div>
    </>
  );
};

export default Toaster;
