import React from 'react'
import Navbar from '../components/Navbar/Navbar'
import Footer from '../components/Footer/Footer'
import './AboutUs.scss'
import Header from '../components/Header/Header'

const AboutUs = () => {
  return (
    <>
      <Navbar />
      <div className="about-us">
        <div>
          <h1>La Gamification du tri de déchets</h1>
          <p>Dans le cadre du Hackaton 2021 pour l'ESTIAM, notre équipe devons réaliser un projet sur La Gamification au service des enjeux de société.</p>
          <p>Nous visons donc à développer une solution ludique visant à responsabiliser l’utilisateur de
            l’importance et de l’impact du recyclage sur le réchauffement climatique.</p>
            <Header />
        </div>
        <h3>Etudiants</h3>
        <div className="students-name">
          <p>Vincent CAMBIER</p>
          <p>Saleck EL JILI</p>
          <p>Yoan MENDES SEMEDO</p>
          <p>Yani MIRA</p>
          <p>Rakym OUATTARA</p>
          <p>Philippe Jacques OUC-HOUANG FOGOUM</p>
          <p>Lory-Stan TANASI</p>
        </div>
      </div>
    </>
  )
}

export default AboutUs
