import React, { useEffect, useState, useRef } from 'react'
import ZoneD from '../components/ZoneD/ZoneD'
import EarthProgressBar from '../components/ProgressBar/EarthProgressBar'
import Navbar from '../components/Navbar/Navbar'
import Footer from '../components/Footer/Footer'
import './Home.scss'
import Wrapper from "../components/Poubelle/Wrapper"
import image1 from '../images/earth-destroyed.jpg'
import image2 from '../images/terre.jpg'
import { useHistory } from 'react-router-dom'
import { BiRefresh } from "react-icons/bi"

const Home = () => {
  const history = useHistory()
  const plastiqueTrash = useRef();
  const [earthPercentage, setEarthPercentage] = useState(90);
  const earthColor = {
    0: "#8a0b0b",
    10: "#7a330c",
    20: "#97521c",
    30: "#935a2d",
    40: "#936d2d",
    50: "#867230",
    60: "#6a6730",
    70: "#5d6a30",
    80: "#346a30",
    90: "#508a4b",
    100: "#71b46b",
  };

  let backgroundColor = earthColor[10 * Math.floor(earthPercentage / 10)];
  let pTop
  let pBottom
  let pRight
  let pLeft
  let mTop
  let mBottom
  let mRight
  let mLeft
  let vTop
  let vBottom
  let vRight
  let vLeft
  let oTop
  let oBottom
  let oRight
  let oLeft
  let paTop
  let paBottom
  let paRight
  let paLeft
  function offset(el, xTop, xBottom, xRight, xLeft) {
    var rect = el.getBoundingClientRect();
    xTop = rect.top
    xBottom = rect.bottom
    xRight = rect.right
    xLeft = rect.left
  }

  useEffect(() => {
    const plastique = document.querySelector(".plastique");
    const metal = document.querySelector(".metal");
    const papier = document.querySelector(".papier");
    const organique = document.querySelector(".organique");
    const verre = document.querySelector(".verre");

    console.log("PLASTIQUE");
    offset(plastique, pTop, pBottom, pRight, pLeft);
    console.log("METAL");
    offset(metal, mTop, mBottom, mRight, mLeft);
    console.log("PAPIER");
    offset(papier, paTop, paBottom, paRight, paLeft);
    console.log("ORGANIQUE");
    offset(organique, oTop, oBottom, oRight, oLeft);
    console.log("VERRE");
    offset(verre, vTop, vBottom, vRight, vLeft);
    console.log(paTop)
  }, []);

  useEffect(() => {
    const timerLoop = setInterval(() => {
      if (earthPercentage > 0) {
        setEarthPercentage(percent => {
          if (percent > 0 && percent < 100) {
            return percent - 2
          } else {
            return percent
          }
        });
      } else {
        clearInterval(timerLoop);
      }
    }, 1000);
  }, [])


  return (
    <>
      <Navbar backgroundColor={backgroundColor} />
      <EarthProgressBar
        progress={earthPercentage} />
      {earthPercentage <= 0 ? (
        <div className="result-game">
          <img src={image1} alt="" />
          <h1>Vous avez perdu</h1>
          <h2>La terre est détruite</h2>
          <BiRefresh className="refresh" onClick={() => window.location.reload()} />
        </div>
      ) :
        earthPercentage >= 100 ? (
          <div className="result-game">
            <img src={image2} alt="" />
            <h1>Vous avez gagné</h1>
            <h2>La terre est sauvé</h2>
            <BiRefresh className="refresh" onClick={() => window.location.reload()} />
          </div>
        ) : (
          <>
            <ZoneD
              pTop={pTop}
              pBottom={pBottom}
              pRight={pRight}
              pLeft={pLeft}
              mTop={mTop}
              mBottom={mBottom}
              mRight={mRight}
              mLeft={mLeft}
              vTop={vTop}
              vBottom={vBottom}
              vRight={vRight}
              vLeft={vLeft}
              oTop={oTop}
              oBottom={oBottom}
              oRight={oRight}
              oLeft={oLeft}
              paTop={paTop}
              paBottom={paBottom}
              paRight={paRight}
              paLeft={paLeft}
              pourcent={earthPercentage}
              setPourcent={setEarthPercentage}
            />
            <Wrapper plastiqueRef={plastiqueTrash} />
          </>
        )}
      <Footer backgroundColor={backgroundColor} />
    </>
  );
};

export default Home;